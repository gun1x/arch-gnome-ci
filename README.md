# CI test for Gnome desktop on Archlinux

*This project is `WORK IN PROGRESS`*

## Phase 1: Testing system upgrade `DONE`

The pipeline is configured to execute a full system update, restart the VM, take a screenshot and compare the result with an expected screen:

```
compare -metric RMSE expected.ppm result.ppm NULL:
```

If the differenace is below 10%, the test is considered a success.

## Phase 2: Testing full install

After the first phase of the project is done, testing of a full install will be started. This means the install has to be fully automated.

Possible method of testing:

1. attach a disk to an archlinux VM
2. install the system to the specific disk, and also install Gnome and GDM
3. detach the disk, attach it to a new VM and boot it
4. after the VM boots, take screenshot and compare it to the standard

For ideas, please contact me at gunix@gunix.cloud or on freenode: gunix
