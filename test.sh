ssh -oStrictHostKeyChecking=no gunix@gnome-testing sudo pacman -Syu --noconfirm
sudo virsh reboot --domain gnome-testing
#wait 30 sec for VM to boot
sleep 30
echo the first screenshot is always bad, so I will take 3 screenshots
sudo /usr/bin/virsh screenshot --file result.ppm gnome-testing
sleep 5
sudo /usr/bin/virsh screenshot --file result.ppm gnome-testing
sleep 5
sudo /usr/bin/virsh screenshot --file result.ppm gnome-testing
#do a bash trick to suppress the 1 error code from compare
percentage=$(echo $( compare -metric RMSE expected.ppm result.ppm NULL: 2>&1 || true ) | cut -d" " -f2 | grep -oE "[0-9\.]*")
echo I compared result.ppm with expected.ppm and the difference is $percentage%
if (( $(echo "$percentage > 0.1" | bc -l) )); then
   echo There is a big difference between how it should look and how it looks. Please inspect VM health.
   exit 1
fi

echo "Considering the difference is below 10%, I consider this to be a success"
